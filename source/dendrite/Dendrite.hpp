#ifndef DENDRITE_CASE_H
#define DENDRITE_CASE_H

#include <medusa/Medusa.hpp>
#include "../helpers/helper.hpp"
#include "../helpers/vtk.hpp"
#include "../helpers/SplineShape.hpp"
#include "../TipVelocity/fTipVelocity.h"
#include "../TipVelocity/fCaVo.cpp"
#include "../TipVelocity/fTipVelocity.cpp"

using namespace std;
using namespace mm;

template <typename vec_t>
struct Dendrite {
  public:
    static void Solve(const XML& conf, HDF& hdf, DomainDiscretization<vec_t>& domain) {
        string solve_problem_type = conf.get<string>("simulation.problem_type");

        if (solve_problem_type == "heat_only") {
            SolveHeatTransferOnly(conf, hdf, domain);
        } else if (solve_problem_type == "heat_and_interpolation") {
            SolveHeatTransferAndInterpolate(conf, hdf, domain);
        } else if (solve_problem_type == "complete") {
            SolveForGrowingDendrite(conf, hdf, domain);
        } else {
            assert_msg(false,
                       "Unknown problem type. Possible types: 'heat_only', "
                       "'heat_and_interpolation' and 'complete'.");
        }
    };

  private:
    static void SolveHeatTransferOnly(const XML& conf, HDF& hdf,
                                      DomainDiscretization<vec_t>& domain) {
        if (conf.get<int>("debug.print") == 1) {
            cout << "Running heat transfer only ..." << endl;
        }

        // Times.
        auto dt = conf.get<double>("simulation.dt");
        auto simulation_time = conf.get<double>("simulation.total_time");
        auto time_steps = ceil(simulation_time / dt);

        // Find suport size.
        auto support_size = conf.get<int>("engine.support_size");
        auto interior = domain.interior();
        auto neu = domain.types() == conf.get<int>("liquid.type");
        auto dir = domain.types() == conf.get<int>("dendrite.type");
        ComputeSupportsFordomain(domain, support_size, neu, dir);

        // Shapes.
        auto monomial_degree = conf.get<int>("engine.monomial_degree");
        Polyharmonic<double, 3> ph;
        RBFFD<decltype(ph), vec_t, ScaleToFarthest> appr(ph, Monomials<vec_t>(monomial_degree));
        auto storage = domain.template computeShapes(appr);
        auto op = storage.explicitOperators();

        // Problem.
        ScalarFieldd u(domain.size());
        auto lambda = conf.get<double>("liquid.k") /
                      (conf.get<double>("liquid.rho") * conf.get<double>("liquid.cp"));
        for (int time_step = 0; time_step <= time_steps; time_step++) {
            // Compute simulation time.
            double time = time_step * dt;

            if (time_step == 0) {
                // Set initial condition.
                SetInitialConditions(u, conf, dir);
            } else {
#pragma omp parallel for
                for (int i : interior) {
                    u[i] += dt * lambda * op.lap(u, i);
                }

                // Enforce neumann boundary condition.
                if (conf.get<int>("simulation.neumann") == 1) {
                    for (int i : neu) {
                        u[i] = op.neumann(u, i, domain.normal(i), 0.0);
                    }
                } else {
                    SetDirichletBC(u, conf.get<double>("liquid.temperature"), neu);
                }

                SetDirichletBC(u, conf.get<double>("dendrite.temperature"), dir);
            }

            if (time_step % conf.get<int>("meta.save_every") == 0 || time_step == time_steps) {
                if (conf.get<int>("debug.print") == 1) {
                    prn(time_step);
                }

                hdf.setGroupName(format("step_%06d", time_step));
                hdf.atomic().writeDomain("domain", domain);
                hdf.atomic().writeDoubleArray("solution", u);
                hdf.atomic().writeDoubleAttribute("time", time);

                // Save results to VTK.
                std::stringstream fname;
                fname << conf.get<string>("meta.filename_out");

                vtk::outputVTK vtk(domain, "Phase change simulation", fname.str().c_str(),
                                   time_step, "../data");
                vtk.addScalar(u, "temperature");
                vtk.addScalar(domain.types(), "type");
                vtk.close();

                cout << " Saving completed." << endl;
            }
        }
    };

    static void SolveHeatTransferAndInterpolate(const XML& conf, HDF& hdf,
                                                DomainDiscretization<vec_t>& domain) {
        if (conf.get<int>("debug.print") == 1) {
            cout << "Running heat transfer with interpolation ..." << endl;
        }

        // Times.
        auto dt = conf.get<double>("simulation.dt");
        auto simulation_time = conf.get<double>("simulation.total_time");
        auto time_steps = ceil(simulation_time / dt);

        // Shapes.
        auto monomial_degree = conf.get<int>("engine.monomial_degree");
        Polyharmonic<double, 3> ph;
        RBFFD<decltype(ph), vec_t, ScaleToFarthest> appr(ph, Monomials<vec_t>(monomial_degree));

        // Problem.
        auto domain_old = domain;
        ScalarFieldd u(domain.size()), u_old;
        KDTree<vec_t> tree(domain.positions());
        KDTree<vec_t> tree_old;
        auto lambda = conf.get<double>("liquid.k") /
                      (conf.get<double>("liquid.rho") * conf.get<double>("liquid.cp"));
        for (int time_step = 0; time_step <= time_steps; time_step++) {
            // Find suport size.
            auto support_size = conf.get<int>("engine.support_size");
            auto interior = domain.interior();
            auto neu = domain.types() == conf.get<int>("liquid.type");
            auto dir = domain.types() == conf.get<int>("dendrite.type");
            ComputeSupportsFordomain(domain, support_size, neu, dir);

            auto storage = domain.template computeShapes(appr);
            auto op = storage.explicitOperators();

            // Compute simulation time.
            double time = time_step * dt;

            if (time_step == 0) {
                // Set initial condition.
                SetInitialConditions(u, conf, dir);
            } else {
                ScalarFieldd u_new(u.size());
#pragma omp parallel for
                for (int i : interior) {
                    u_new[i] = u[i] + dt * lambda * op.lap(u, i);
                }

                // Enforce neumann boundary condition.
                if (conf.get<int>("simulation.neumann") == 1) {
                    for (int i : neu) {
                        u_new[i] = op.neumann(u, i, domain.normal(i), 0.0);
                    }
                } else {
                    SetDirichletBC(u_new, conf.get<double>("model.omega0"), neu);
                }

                SetDirichletBC(u_new, conf.get<double>("dendrite.temperature"), dir);

                for (int i = 0; i < u.size(); i++) {
                    u[i] = u_new[i];
                }
            }

            // Save if required.
            if (time_step % conf.get<int>("meta.save_every") == 0 || time_step == time_steps) {
                prn(time_step);

                hdf.setGroupName(format("step_%06d", time_step));
                hdf.atomic().writeDomain("domain", domain);
                hdf.atomic().writeDoubleArray("solution", u);
                hdf.atomic().writeDoubleAttribute("time", time);

                // Save results to VTK.
                std::stringstream fname;
                fname << conf.get<string>("meta.filename_out");

                vtk::outputVTK vtk(domain, "Phase change simulation", fname.str().c_str(),
                                   time_step, "../data");
                vtk.addScalar(u, "temperature");
                vtk.addScalar(domain.types(), "type");
                vtk.close();

                cout << " Saving completed." << endl;
            }

            // Obtain new domaina and scalar field.
            domain_old = domain;
            tree_old.reset(domain_old.positions());
            if (conf.get<int>("debug.print") == 1) {
                cout << "Obtaining new discretization ..." << endl;
            }
            GetNewDiscretization(domain, conf);
            tree.reset(domain.positions());

            // Map solution to new discretization.
            if (conf.get<int>("debug.print") == 1) {
                cout << "Mapping solution to new discretization ..." << endl;
            }
            u_old = u;
            u = MapSolutionToNewDiscretization(u_old, domain_old, domain, tree, tree_old, conf,
                                               true);
        }
    };

    /**
     * @brief Growing dendrite simulation.
     *
     * @param conf Configuration file.
     * @param hdf Output file.
     * @param domain Domain.
     */
    static void SolveForGrowingDendrite(const XML& conf, HDF& hdf,
                                        DomainDiscretization<vec_t>& domain) {
        if (conf.get<int>("debug.print") == 1) {
            cout << "Running dendrite growth simulation ..." << endl;
        }

        // Times.
        auto dt = conf.get<double>("simulation.dt");
        auto simulation_time = conf.get<double>("simulation.total_time");
        auto time_steps = ceil(simulation_time / dt);

        // Engine for shapes.
        auto monomial_degree = conf.get<int>("engine.monomial_degree");
        auto sigma_w = conf.get<int>("engine.sigma_w");
        using MatrixType = Eigen::Matrix<typename vec_t::scalar_t, Eigen::Dynamic, Eigen::Dynamic>;
        // Polyharmonic<double, 3> ph;
        // RBFFD<decltype(ph), vec_t, ScaleToFarthest> appr(ph, Monomials<vec_t>(monomial_degree));
        WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest,
            Eigen::ColPivHouseholderQR<MatrixType>>
            appr(Monomials<vec_t>(monomial_degree), sigma_w);

        // Problem.
        auto domain_old = domain;
        Range<double> tip_velocities(domain.size()), omega_deltas(domain.size());
        for (int i = 0; i < domain.size(); i++) {
            tip_velocities[i] = 0.0;
            omega_deltas[i] = 0.0;
        }
        ScalarFieldd u(domain.size()), u_old;
        KDTree<vec_t> tree(domain.positions());
        // KDTree<vec_t> tree_old;
        auto lambda = conf.get<double>("liquid.k") /
                      (conf.get<double>("liquid.rho") * conf.get<double>("liquid.cp"));
        for (int time_step = 0; time_step <= time_steps; time_step++) {
            // prn("***************************");
            // prn(time_step);
            // Timer t;
            // t.addCheckPoint("start");
            // Find suport size.
            auto support_size = conf.get<int>("engine.support_size");
            auto interior = domain.interior();
            auto neu = domain.types() == conf.get<int>("liquid.type");
            auto dir = domain.types() == conf.get<int>("dendrite.type");
            // t.addCheckPoint("support_start");
            ComputeSupportsFordomain(domain, support_size, neu, dir);
            // t.addCheckPoint("support_end");
            // prn(t.duration("support_start", "support_end"));

            // t.addCheckPoint("shape_start");
            auto storage = domain.template computeShapes(appr);
            // t.addCheckPoint("shape_end");
            // prn(t.duration("shape_start", "shape_end"));
            // t.addCheckPoint("op_start");
            auto op = storage.explicitOperators();
            // t.addCheckPoint("op_end");
            // prn(t.duration("op_start", "op_end"));

            // Compute simulation time.
            double time = time_step * dt;

            if (time_step == 0) {
                // Set initial condition.
                SetInitialConditions(u, conf, dir);
            } else {
                // t.addCheckPoint("problem_start");
                ScalarFieldd u_new(u.size());
#pragma omp parallel for
                for (int i : interior) {
                    u_new[i] = u[i] + dt * lambda * op.lap(u, i);
                }
                // t.addCheckPoint("problem_end");
                // prn(t.duration("problem_start", "problem_end"));

                // t.addCheckPoint("BC_start");
                // Enforce neumann boundary condition.
                if (conf.get<int>("simulation.neumann") == 1) {
                    for (int i : neu) {
                        u_new[i] = op.neumann(u, i, domain.normal(i), 0.0);
                    }
                } else {
                    SetDirichletBC(u_new, conf.get<double>("model.omega0"), neu);
                }

                SetDirichletBC(u_new, conf.get<double>("dendrite.temperature"), dir);
                // t.addCheckPoint("BC_end");
                // prn(t.duration("BC_start", "BC_end"));
#pragma omp parallel for
                for (int i = 0; i < u.size(); i++) {
                    u[i] = u_new[i];
                }
            }

            // Save if required.
            if (time_step % conf.get<int>("meta.save_every") == 0 || time_step == time_steps) {
                // prn(time_step);
                // prn(time);
                cout << "---------" << endl;
                SaveToFile(hdf, u, u_old, domain, domain_old, conf, time_step, tip_velocities,
                           omega_deltas, time, conf.get<int>("meta.save_to_vtk") == 1);
            }

            // Obtain new domain and scalar field.
            domain_old = domain;
            u_old = u;
            // tree_old.reset(domain_old.positions());
            if (conf.get<int>("debug.print") == 1) {
                cout << "Obtaining new discretization ..." << endl;
            }
            ResetRanges(tip_velocities, omega_deltas, domain.size());
            // auto new_boundary = ObtainNewDendriteBoundary(domain_old, u_old, tree_old, conf,
            //                                               tip_velocities, omega_deltas);
            // t.addCheckPoint("boundary_start");
            auto new_boundary = ObtainNewDendriteBoundary(domain_old, u_old, tree, conf,
                                                          tip_velocities, omega_deltas);
            // t.addCheckPoint("boundary_end");
            // prn(t.duration("boundary_start", "boundary_end"));

            // t.addCheckPoint("discretize_start");
            bool rebuild_domain = time_step % conf.get<int>("growth.rebuild_domain_every") == 0;
            GetNewDiscretizationFromBoundary(domain, conf, new_boundary, rebuild_domain);
            // t.addCheckPoint("discretize_end");
            // prn(t.duration("discretize_start", "discretize_end"));

            // t.addCheckPoint("tree_start");
            tree.reset(domain.positions());
            // t.addCheckPoint("tree_end");
            // prn(t.duration("tree_start", "tree_end"));

            // Map solution to new discretization.
            if (conf.get<int>("debug.print") == 1) {
                cout << "Mapping solution to new discretization ..." << endl;
            }
            // u = MapSolutionToNewDiscretization(u_old, domain_old, domain, tree, tree_old,
            // conf);
            // t.addCheckPoint("map_start");
            u = MapSolutionToNewDiscretization(u_old, domain_old, domain, tree, tree, conf,
                                               rebuild_domain);
            // t.addCheckPoint("map_end");
            // prn(t.duration("map_start", "map_end"));

            // t.addCheckPoint("end");
            // prn(t.duration("start", "end"));
        }
    };
    //
    //
    //
    //
    //
    // Just some comment to separate code.
    //
    //
    //
    //
    //
    //
  private:
    static void ComputeSupportsFordomain(DomainDiscretization<vec_t>& domain, int support_size,
                                         const Range<int>& neu, const Range<int>& dir) {
        domain.findSupport(FindClosest(support_size).forNodes(domain.interior() + dir));
        domain.findSupport(
            FindClosest(support_size).forNodes(neu).searchAmong(domain.interior()).forceSelf(true));
    };

    static void SetInitialConditions(ScalarFieldd& u, const XML& conf, const Range<int>& dir) {
        auto liquid_temp = conf.get<double>("model.omega0");
        auto dendrite_temp = conf.get<double>("dendrite.temperature");

        u.setConstant(liquid_temp);
        SetDirichletBC(u, dendrite_temp, dir);
    };

    static void SetDirichletBC(ScalarFieldd& u, double value, const Range<int>& indexes) {
        for (int i : indexes) {
            u[i] = value;
        };
    };

    static void GetNewDiscretization(DomainDiscretization<vec_t>& domain, const XML& conf) {
        // Get dendrite boundary.
        auto dendrite_indexes = domain.types() == conf.get<int>("dendrite.type");
        Range<vec_t> dendrite_positions = domain.positions()[dendrite_indexes];

        // Call basic function.
        GetNewDiscretizationFromBoundary(domain, conf, dendrite_positions, true);
    };

    static void GetNewDiscretizationFromBoundary(DomainDiscretization<vec_t>& domain,
                                                 const XML& conf, Range<vec_t>& dendrite_positions,
                                                 bool build_new) {
        // Shuffle dendrite points.
        auto rng = std::default_random_engine{};
        std::shuffle(std::begin(dendrite_positions), std::end(dendrite_positions), rng);

        // Dendrite shape.
        SplineShape dendrite_shape(dendrite_positions);
        vec_t centre(vec_t::dim);
        centre.setZero();
        dendrite_shape.setInterior(centre);

        // Dendrite domain.
        auto dendrite_h = conf.get<double>("dendrite.h");
        auto dendrite_type = conf.get<int>("dendrite.type");
        DomainDiscretization<vec_t> domain_dendrite =
            dendrite_shape.discretizeBoundaryWithStep(dendrite_h, dendrite_type);

        // Luquid domain.
        if (build_new) {
            // Clear domain.
            domain.clear();
            auto liquid_h = conf.get<double>("liquid.h");
            if (conf.get<string>("domain.shape") == "box") {
                vec_t corner;
                corner.setConstant(conf.get<double>("liquid.size"));
                BoxShape<vec_t> box(-corner, corner);
                auto d_box = box.discretizeBoundaryWithStep(liquid_h, conf.get<int>("liquid.type"));

                // Domain difference.
                d_box -= domain_dendrite;

                // Assign new domain.
                domain = d_box;
            } else {
                BallShape<vec_t> ball(conf.get<double>("liquid.origin"),
                                      conf.get<double>("liquid.size"));
                auto d_ball =
                    ball.discretizeBoundaryWithStep(liquid_h, conf.get<int>("liquid.type"));

                // Domain difference.
                d_ball -= domain_dendrite;

                // Assign new domain.
                domain = d_ball;
            }

            // Fill.
            KDTree<vec_t> tree_dendrite(domain_dendrite.positions());
            auto fn = [&](const vec_t& p) { return dx(p, tree_dendrite, conf); };
            // Fill engine.
            GeneralFill<vec_t> fill_engine;
            fill_engine.numSamples(conf.get<int>("domain.fill_samples"));
            // Fill domain with nodes.
            domain.fill(fill_engine, fn);
        } else {
            Range<int> nodes_to_remove;
#pragma omp parallel for
            for (int i : domain.interior()) {
                if (domain_dendrite.contains(domain.pos(i))) {
                    nodes_to_remove.push_back(i);
                }
            }

            auto dendrite = domain.types() == dendrite_type;
            domain.removeNodes(dendrite + nodes_to_remove);

            for (int i = 0; i < domain_dendrite.size(); i++) {
                domain.addBoundaryNode(domain_dendrite.pos(i), dendrite_type,
                                       -domain_dendrite.normal(i));
            }
        }

        // Flip normals to point inwards.
        auto dir = domain.types() == conf.get<int>("dendrite.type");
        for (int i : dir) {
            domain.normal(i) = -domain.normal(i);
        }
    };

    static ScalarFieldd MapSolutionToNewDiscretization(ScalarFieldd& u_old,
                                                       DomainDiscretization<vec_t>& domain_old,
                                                       DomainDiscretization<vec_t>& domain,
                                                       KDTree<vec_t>& tree, KDTree<vec_t>& tree_old,
                                                       const XML& conf, bool domain_rebuilt) {
        ScalarFieldd u(domain.size());
        using MatrixType = Eigen::Matrix<typename vec_t::scalar_t, Eigen::Dynamic, Eigen::Dynamic>;

        auto approximation_type = conf.get<string>("field_interpolation.type");

        if (domain_rebuilt) {
            if (approximation_type == "sh") {
                // *********
                // Sheppard.
                // *********
                if (conf.get<int>("debug.print") == 1) {
                    cout << "Mapping to new discretization using Sheppard ..." << endl;
                }
                // Setup interpolant.
                Range<double> values(u_old.size());
#pragma omp parallel for
                for (int i = 0; i < u_old.size(); i++) {
                    values[i] = static_cast<double>(u_old[i]);
                }

                SheppardInterpolant<vec_t, double> interpolant(domain_old.positions(), values);

                int num_closest = conf.get<int>("field_interpolation.closest");
                auto not_dendrite = domain.types() != conf.get<int>("dendrite.type");
#pragma omp parallel for
                for (int i : not_dendrite) {
                    u[i] = interpolant(domain.pos(i), num_closest);
                }
            } else if (approximation_type == "pu") {
                // **
                // PU
                // **
                if (conf.get<int>("debug.print") == 1) {
                    cout << "Mapping to new discretization using PU ..." << endl;
                }
                // Update PU support size.
                auto d_temp = domain_old;
                // d_temp.findSupport(FindClosest(conf.get<int>("field_interpolation.fieldmap_closest")));
                int support_size = conf.get<int>("field_interpolation.closest");
                auto neu = d_temp.types() == conf.get<int>("liquid.type");
                auto dir = d_temp.types() == conf.get<int>("dendrite.type");
                ComputeSupportsFordomain(d_temp, support_size, neu, dir);

                // Values.
                Eigen::VectorXd values(u_old.size());
                for (int i = 0; i < u_old.size(); i++) {
                    values[i] = static_cast<double>(u_old[i]);
                }

                // New values.
                Eigen::VectorXd new_values(domain.size());

                // Approximation engine.
                auto approximation_engine = conf.get<string>("field_interpolation.engine");

                if (approximation_engine == "wls") {
                    // WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> appr2(
                    // Monomials<vec_t>(conf.get<int>("field_interpolation.m")),
                    // conf.get<double>("field_interpolation.sigma_w"));
                    WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest,
                        Eigen::ColPivHouseholderQR<MatrixType>>
                        appr2(Monomials<vec_t>(conf.get<int>("field_interpolation.m")),
                              conf.get<double>("field_interpolation.sigma_w"));

                    new_values = PUApproximant<vec_t>::evaluate(
                        d_temp, values, tree, conf.get<double>("field_interpolation.eff_radius"),
                        appr2);
                } else if (approximation_engine == "rbffd") {
                    RBFFD<Polyharmonic<double>, vec_t, ScaleToClosest> appr2(
                        {3}, Monomials<vec_t>(conf.get<int>("field_interpolation.m")));
                    new_values = PUApproximant<vec_t>::evaluate(
                        d_temp, values, tree, conf.get<double>("field_interpolation.eff_radius"),
                        appr2);
                } else {
                    assert_msg(false,
                               "Unexpected field approximation engine. Use 'wls' or 'rbffd'.");
                }

                for (int i = 0; i < new_values.size(); i++) {
                    u[i] = new_values[i];
                }
            }
            // else if (approximation_type == "closest") {
            //     // ********
            //     // Closest.
            //     // ********
            //     if (conf.get<int>("debug.print") == 1) {
            //         cout << "Mapping to new discretization using closest ..." << endl;
            //     }
            //     // Loop over new domain positions. For each position find closest node from
            //     old domain
            //     // and assign its value to new position.

            //     Range<double> distances2;  // squares of distances
            //     Range<int> closest;

            //     for (int i = 0; i < domain.size(); i++) {
            //         auto pos = domain.pos(i);
            //         std::tie(closest, distances2) = tree_old.query(pos, 1);
            //         int closest_idx = closest[0];
            //         u[i] = u_old[closest_idx];
            //     }
            // }
            else {
                assert_msg(false, "Unknown field interpolation type. Use 'pu', 'sh' or 'closest'.");
            }
        } else {
#pragma omp parallel for
            for (int i = 0; i < domain.size(); i++) {
                auto new_pos = domain.pos(i);
                for (int j = 0; j < domain_old.size(); j++) {
                    auto old_pos = domain_old.pos(j);

                    auto len = (new_pos - old_pos).norm();

                    if (len < 1e-6) {
                        u[i] = u_old[j];
                    }
                }
            }
        }

        // Enforce Dirichlet BC.
        auto dir = domain.types() == conf.get<int>("dendrite.type");
        auto dendrite_temp = conf.get<double>("dendrite.temperature");
        SetDirichletBC(u, dendrite_temp, dir);

        return u;
    };

    static void SaveToFile(HDF& hdf, const ScalarFieldd& u, const ScalarFieldd& u_old,
                           const DomainDiscretization<vec_t>& domain,
                           const DomainDiscretization<vec_t>& domain_old, const XML& conf,
                           const int time_step, const Range<double>& tip_velocities,
                           const Range<double>& omega_deltas, const double time, bool save_to_vtk) {
        cout << "Saving ..." << endl;
        // Timer t;

        // t.addCheckPoint("HDF_start");
        hdf.setGroupName(format("step_%06d", time_step));
        hdf.atomic().writeDomain("domain", domain);
        hdf.atomic().writeDomain("domain_old", domain_old);
        hdf.atomic().writeDoubleArray("solution", u);
        hdf.atomic().writeDoubleArray("tip_velocities", tip_velocities);
        hdf.atomic().writeDoubleArray("omega_deltas", omega_deltas);
        hdf.atomic().writeDoubleAttribute("time", time);
        double max_tip_velocity = *std::max_element(tip_velocities.begin(), tip_velocities.end());
        hdf.atomic().writeDoubleAttribute("max_tip_velocity", max_tip_velocity);
        // t.addCheckPoint("HDF_end");
        // prn(t.duration("HDF_start", "HDF_end"));

        if (save_to_vtk){
            // Save results to VTK.
            // t.addCheckPoint("VTK_start");
            std::stringstream fname;
            fname << conf.get<string>("meta.filename_out");

            vtk::outputVTK vtk(domain, "Phase change simulation", fname.str().c_str(), time_step,
                            "../data");
            vtk.addScalar(u, "temperature");
            vtk.addScalar(domain.types(), "type");
            vtk.close();

            fname << "_v";
            vtk::outputVTK vtk_v(domain_old, "Phase change simulation", fname.str().c_str(), time_step,
                                "../data");
            vtk_v.addScalar(tip_velocities, "tip_velocities");
            vtk_v.addScalar(omega_deltas, "omega_deltas");
            vtk_v.close();
            // t.addCheckPoint("VTK_end");
            // prn(t.duration("VTK_start", "VTK_end"));
        }
        cout << " Saving completed." << endl;
    };

    static Range<vec_t> ObtainNewDendriteBoundary(const DomainDiscretization<vec_t>& domain,
                                                  const ScalarFieldd& u, KDTree<vec_t>& tree,
                                                  const XML& conf, Range<double>& tip_velocities,
                                                  Range<double>& omega_deltas) {
        using MatrixType = Eigen::Matrix<typename vec_t::scalar_t, Eigen::Dynamic, Eigen::Dynamic>;
        // Params.
        auto dendrite_type = conf.get<int>("dendrite.type");
        auto delta = conf.get<double>("model.delta");
        auto time_step = conf.get<double>("simulation.dt");

        // Current dendrite boundary.
        auto dendrite_indexes = domain.types() == dendrite_type;
        int N_dendrite = dendrite_indexes.size();

        // Compute temperature positions.
        Range<vec_t> temperature_positions(N_dendrite);
        for (int i = 0; i < N_dendrite; i++) {
            int domain_idx = dendrite_indexes[i];

            auto pos = domain.pos(domain_idx);
            auto normal = domain.normal(domain_idx);
            normal = -normal.normalized();  // Flip normal.

            temperature_positions[i] = pos + delta * normal;
        }

        // Compute temperatures using interpolants.
        string interpolation_type = conf.get<string>("temperature_interpolation.type");
        Range<double> delta_temperatures(N_dendrite);
        if (interpolation_type == "sh") {
            // *********
            // Sheppard.
            // *********
            if (conf.get<int>("debug.print") == 1) {
                cout << "Obtaining delta temperature using Sheppard ..." << endl;
            }
            // Setup interpolant.
            Range<double> values(u.size());
#pragma omp parallel for
            for (int i = 0; i < u.size(); i++) {
                values[i] = static_cast<double>(u[i]);
            }

            SheppardInterpolant<vec_t, double> interpolant(domain.positions(), values);

            int num_closest = conf.get<int>("temperature_interpolation.closest");
#pragma omp parallel for
            for (int i = 0; i < N_dendrite; i++) {
                delta_temperatures[i] = interpolant(temperature_positions[i], num_closest);
            }
        } else if (interpolation_type == "pu") {
            // **
            // PU
            // **
            if (conf.get<int>("debug.print") == 1) {
                cout << "Obtaining delta temperature using PU ..." << endl;
            }
            // Update PU support size.
            auto d_temp = domain;
            // d_temp.findSupport(FindClosest(conf.get<int>("field_interpolation.fieldmap_closest")));
            int support_size = conf.get<int>("temperature_interpolation.closest");
            auto neu = d_temp.types() == conf.get<int>("liquid.type");
            auto dir = d_temp.types() == conf.get<int>("dendrite.type");
            ComputeSupportsFordomain(d_temp, support_size, neu, dir);

            // Values.
            Eigen::VectorXd values(u.size());
#pragma omp parallel for
            for (int i = 0; i < u.size(); i++) {
                values[i] = static_cast<double>(u[i]);
            }

            // New values.
            Eigen::VectorXd new_values(N_dendrite);

            // Approximation engine.
            auto approximation_engine = conf.get<string>("temperature_interpolation.engine");

            if (approximation_engine == "wls") {
                WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest,
                    Eigen::ColPivHouseholderQR<MatrixType>>
                    appr2(Monomials<vec_t>(conf.get<int>("temperature_interpolation.m")),
                          conf.get<double>("temperature_interpolation.sigma_w"));
                // WLS<Gaussians<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> appr2(
                //     {support_size, 100.0}, 1.);
                new_values = PUApproximant<vec_t>::evaluate(
                    d_temp, values, temperature_positions,
                    conf.get<double>("temperature_interpolation.eff_radius"), appr2);
            } else if (approximation_engine == "rbffd") {
                RBFFD<Polyharmonic<double>, vec_t, ScaleToClosest> appr2(
                    {3}, Monomials<vec_t>(conf.get<int>("temperature_interpolation.m")));
                new_values = PUApproximant<vec_t>::evaluate(
                    d_temp, values, temperature_positions,
                    conf.get<double>("temperature_interpolation.eff_radius"), appr2);
            } else {
                assert_msg(false, "Unexpected field approximation engine. Use 'wls' or 'rbffd'.");
            }
            for (int i = 0; i < new_values.size(); i++) {
                delta_temperatures[i] = new_values[i];
            }
        }
        // else if (interpolation_type == "closest") {
        //     // ********
        //     // Closest.
        //     // ********
        //     if (conf.get<int>("debug.print") == 1) {
        //         cout << "Obtaining delta temperature using closest ..." << endl;
        //     }
        //     // Loop over new domain positions. For each position find closest node from old
        //     domain
        //     // and assign its value to new position.

        //     Range<double> distances2;  // squares of distances
        //     Range<int> closest;

        //     for (int i = 0; i < N_dendrite; i++) {
        //         auto pos = temperature_positions[i];
        //         std::tie(closest, distances2) = tree.query(pos, 1);
        //         int closest_idx = closest[0];
        //         delta_temperatures[i] = u[closest_idx];
        //     }
        // }
        else {
            assert_msg(false, "Unknown field interpolation type. Use 'pu', 'sh' or 'closest'.");
        }

        // Move boundary nodes.
        Range<vec_t> new_boundary(N_dendrite);
#pragma omp parallel for
        for (int i = 0; i < N_dendrite; i++) {
            int domain_idx = dendrite_indexes[i];

            auto pos = domain.pos(domain_idx);
            auto normal = domain.normal(domain_idx);
            normal = -normal.normalized();  // Flip normal.

            auto temp_at_delta = delta_temperatures[i];
            omega_deltas[domain_idx] = temp_at_delta;

            auto v_tip = GetVelocity(conf, temp_at_delta);
            tip_velocities[domain_idx] = v_tip;
            auto phi = GetSmallestAngle<vec_t>(normal);

            vec_t new_position = pos + time_step * v_tip * cos(phi) * normal;
            new_boundary[i] = new_position;
        }

        return new_boundary;
    };

    static double GetVelocity(const XML& conf, double temp) {
        const auto omega0 = conf.get<double>("model.omega0");
        const auto delta = conf.get<double>("model.delta");

        vector<double> IvState(3);
        IvState = IvTip(omega0);
        const double VIv = IvState[0];
        const double PeIv = IvState[2];

        return fTipVelocity(temp, delta, omega0, PeIv, VIv);
    };

    static void ResetRanges(Range<double>& r1, Range<double>& r2, int N) {
        r1.clear();
        r2.clear();
        Range<double> _temp(N);
#pragma omp parallel for
        for (int i = 0; i < N; i++) {
            _temp[i] = 0.0;
        }
        r1 = _temp;
        r2 = _temp;
    }
};

#endif