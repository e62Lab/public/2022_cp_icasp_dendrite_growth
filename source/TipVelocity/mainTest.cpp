#include <math.h>
#include <cmath>
#include <vector>
#include <iostream>
#include <fstream>
using namespace std;
#include "fTipVelocity.h"


int main()
{
//***INIT-START****** Tole poklices 1x na zacetku simulacije *********
// Omega0 je brezdimenzijska temperatura v kapljevini pri casu t=0 (zacetni pogoj).
// Vzemi Omega0=0.05
// Ce uproabljas na zunanjem robu domene Dirichletov r.p., naj bo brezdim. temperatura na robu enaka Omega0
// Lahko pa uporabis tudi Neumannov r.p. z gradientom 0.
// Brezdimenzijska temperatura na robu dendrita je 0.

	//*** Case parameter (initial condition)
	//*** This is a constant for each simulation
 	const double Omega0=0.05;

// VIv, RIv in PeIv so referencne vrednosti, ki jih potrebujemo pri klicu Vtip_dimless.
  std::vector<double> IvState(3);
  IvState = IvTip(Omega0);
  const double VIv = IvState[0];
  const double RIv = IvState[1];
	const double PeIv= IvState[2];
//***INIT-END*********************************************************



//***BLABLABLA****** Karkoli *****************************************
	ofstream outfile;
	outfile.open("Vtip_dimless_Om5e-2_CHK.txt");
  outfile << "Omega_delta, delta_dimless, Vtip_dimless" << endl;

	//*** Model parameter of the GEM (Grain Envelope Model)
	//*** This is a constant for each simulation
 	double delta_dimless;
	double Omega_delta;

	// Calculate dimensionless tip velocity at the envelope
	// Calculates a map of dimensionless tip velocity for 0<Omega_delta<Omega0 and 0.1<delta_dimless<2
  for( Omega_delta = Omega0/50.0; Omega_delta < Omega0; Omega_delta = Omega_delta + Omega0/50.0 ) 
  {
		for( delta_dimless = 0.1; delta_dimless < 2.0; delta_dimless = delta_dimless + 1.e-1 )
		{
//***CALL Vtip_dimless ***********************************************
// Vtip_dimless: brezdimenzijska hitrost fronte (roba dendrita)
// Omega_delta: brezdimenzijska temperatura na brezdimenzijski razdalji delta_dimless od fronte
// delta_dimless: brezdimenzijska hitrost fronte (roba dendrita)
// Omega0, PeIv, VIv: referencne velicine izracunane zgoraj
		  double Vtip_dimless = fTipVelocity(Omega_delta, delta_dimless, Omega0, PeIv, VIv);
//***CALL Vtip_dimless ***********************************************
	    outfile  << Omega_delta << "," << delta_dimless << "," << Vtip_dimless << endl;
		}
	}
//***BLABLABLA****** Karkoli *****************************************

}





// 	double OmegaCellI=0.05;
// 
// 	ofstream outfile;
// 	outfile.open("Iv2D.txt");
// // 	outfile.open("Iv3D.txt");
// 	outfile << "Om, Petip, Vtip, Rtip, VtipERR, RtipERR, OmERR" << endl;
// 
// 	for( OmegaCellI = 0.01; OmegaCellI < 1.0; OmegaCellI = OmegaCellI + 1.e-2 ) 
// 	{
// 	    vector<double> Solution(2);
//     	Solution = CantorVogel2DNewton
// //     	Solution = CantorVogel3DNewton
// 	    (
//     	    OmegaCellI,
//         	kp0,
// 	        Gibbs,
//     	    liqSlope,
//         	envelopeClint,
// 	        delta,
//     	    D_liq,
//         	sigma
// 	    );
//     
//     	double d0=Gibbs/(liqSlope*envelopeClint*(kp0-1.0));
// 	    double VtipERR=4.0*sigma*D_liq*Petip*Petip/d0/Vtip-1.0;
//     	double RtipERR=d0/(2.0*sigma*Petip)/Rtip-1.0;
// 	    double OmERR=systemCantorVogel3D(Petip, Solution)/OmegaCellI;

    
   
/*            
  cout << "Petip= "   << Petip << endl;
  cout << "Vtip= "    << Vtip << endl;
  cout << "Rtip= "    << Rtip << endl;
  cout << "VtipERR= " << VtipERR << endl;
  cout << "RtipERR= " << RtipERR << endl;
  cout << "OmERR= "   << OmERR << endl;
*/

// 		outfile  << OmegaCellI << "," << Petip << "," << Vtip << "," << Rtip << "," << VtipERR << "," << RtipERR << "," << OmERR << endl;
//   }
//   outfile.close();
// 
// }
