#ifndef DENDRITE_GROWTH_HELPER_H
#define DENDRITE_GROWTH_HELPER_H

#include <medusa/Medusa_fwd.hpp>
// #include "SplineShape.hpp"
// #include "vtk.hpp"

using namespace std;
using namespace mm;

template <typename vec_t>
double dx(const vec_t& p, KDTree<vec_t>& dendrite_tree, const XML& conf) {
    Range<double> distances2;
    Range<int> closest;
    auto domain_size = conf.get<double>("liquid.size");
    auto dendrite_h = conf.get<double>("dendrite.h");
    auto liquid_h = conf.get<double>("liquid.h");

    // Get 2 points closest to p on dendrite boundary.
    std::tie(closest, distances2) = dendrite_tree.query(p, 2);
    vec_t p_dendrite = dendrite_tree.get(closest[1]);  // Coordinates of the closest point.

    // Find point on liquid boundary in the direction of p_dendrite -> p.
    double a = (p - p_dendrite).dot(p - p_dendrite);
    double b = 2 * (p - p_dendrite).dot(p_dendrite);
    double c = p_dendrite.dot(p_dendrite) - domain_size * domain_size;
    double t1 = (-b + sqrt(b * b - 4 * a * c)) / (2 * a);
    double t2 = (-b - sqrt(b * b - 4 * a * c)) / (2 * a);
    double t = max(t1, t2);

    vec_t p_liquid = p_dendrite + t * (p - p_dendrite);

    return dendrite_h +
           (liquid_h - dendrite_h) * (p - p_dendrite).norm() / (p_liquid - p_dendrite).norm();
}

template <typename vec_t>
double GetSmallestAngle(vec_t normal) {
    vector<tuple<double, vec_t>> directions;
    directions.push_back(tuple<double, vec_t>(0.0, {1, 0}));
    directions.push_back(tuple<double, vec_t>(0.0, {0, 1}));
    directions.push_back(tuple<double, vec_t>(0.0, {-1, 0}));
    directions.push_back(tuple<double, vec_t>(0.0, {0, -1}));

    // Compute angles between normal and unit vectors.
    for (tuple<double, vec_t>& tup : directions) {
        vec_t e = get<1>(tup);
        double phi = acos(e.dot(normal));
        get<0>(tup) = phi;
    }

    // Find minimum angle to determine velocity direction.
    sort(directions.begin(),
         directions.end());  // Sort tuples. Default by first tuple.
    double phi = get<0>(directions[0]);

    return phi;
}
#endif
