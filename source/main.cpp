#include <medusa/Medusa_fwd.hpp>

#include "dendrite/Dendrite.hpp"
#include "helpers/helper.hpp"

using namespace mm;
using namespace std;

template <typename vec_t>
void StartSimulation(const XML& conf, HDF& hdf) {
    // Get domain properties.
    auto domain_shape = conf.get<string>("domain.shape");
    // Get dendrite domain properties.
    auto dendrite_origin = conf.get<double>("dendrite.origin");
    auto dendrite_size = conf.get<double>("dendrite.size");
    auto dendrite_type = conf.get<double>("dendrite.type");
    auto dendrite_h = conf.get<double>("dendrite.h");
    // Get liquid domain properties.
    auto liquid_origin = conf.get<double>("liquid.origin");
    auto liquid_size = conf.get<double>("liquid.size");
    auto liquid_type = conf.get<double>("liquid.type");
    auto liquid_h = conf.get<double>("liquid.h");

    if (domain_shape == "box") {
        if (conf.get<int>("debug.print") == 1) {
            cout << "Creating box shaped domain ..." << endl;
        }

        // Dendrite.
        // Eigen::Rotation2Dd Q(PI / 4);
        vec_t corner;
        // corner.setConstant(dendrite_size);
        // BoxShape<vec_t> dendrite_shape(-corner, corner);
        // DomainDiscretization<vec_t> dendrite_domain =
        //     dendrite_shape.discretizeBoundaryWithStep(dendrite_h, dendrite_type).rotate(Q.toRotationMatrix());;
        BallShape<vec_t> dendrite_shape(dendrite_origin, dendrite_size);
        DomainDiscretization<vec_t> dendrite_domain =
            dendrite_shape.discretizeBoundaryWithStep(dendrite_h, dendrite_type);

        // Liquid.
        corner.setConstant(liquid_size);
        BoxShape<vec_t> liquid_shape(-corner, corner);
        DomainDiscretization<vec_t> domain =
            liquid_shape.discretizeBoundaryWithStep(liquid_h, liquid_type);

        // Domain difference.
        domain -= dendrite_domain;

        // Fill domain.
        KDTree<vec_t> tree(dendrite_domain.positions());
        auto fn = [&](const vec_t& p) { return dx(p, tree, conf); };
        // Fill engine.
        GeneralFill<vec_t> fill;
        fill.numSamples(conf.get<int>("domain.fill_samples"))
            .seed(conf.get<int>("domain.fill_seed"));
        // Fill domain with nodes.
        domain.fill(fill, fn);

        // Run simulation.
        if (conf.get<int>("debug.print") == 1) {
            cout << "Running simulation ..." << endl;
        }
        Dendrite<vec_t>::Solve(conf, hdf, domain);

    } else if (domain_shape == "ball") {
        if (conf.get<int>("debug.print") == 1) {
            cout << "Creating ball shaped domain ..." << endl;
        }

        // Dendrite.
        BallShape<vec_t> dendrite_shape(dendrite_origin, dendrite_size);
        DomainDiscretization<vec_t> dendrite_domain =
            dendrite_shape.discretizeBoundaryWithStep(dendrite_h, dendrite_type);

        // Liquid.
        BallShape<vec_t> liquid_shape(liquid_origin, liquid_size);
        DomainDiscretization<vec_t> domain =
            liquid_shape.discretizeBoundaryWithStep(liquid_h, liquid_type);

        // Domain difference.
        domain -= dendrite_domain;

        // Fill domain.
        KDTree<vec_t> tree(dendrite_domain.positions());
        auto fn = [&](const vec_t& p) { return dx(p, tree, conf); };
        // Fill engine.
        GeneralFill<vec_t> fill;
        fill.numSamples(conf.get<int>("domain.fill_samples"))
            .seed(conf.get<int>("domain.fill_seed"));
        // Fill domain with nodes.
        domain.fill(fill, fn);

        // Run simulation.
        if (conf.get<int>("debug.print") == 1) {
            cout << "Running simulation ..." << endl;
        }
        Dendrite<vec_t>::Solve(conf, hdf, domain);
    } else {
        assert_msg(false, "Unknown domain shape. Use 'box' or 'ball'.");
    }
}

int main(int argc, char* argv[]) {
    // Check for settings file.
    assert_msg(argc >= 2, "Second argument should be the XML parameter file.");

    // Timer initialization.
    Timer t;
    t.addCheckPoint("start");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Set num threads.
    omp_set_num_threads(conf.get<int>("sys.num_threads"));

    // Create H5 file to store the parameters.
    string output_file =
        conf.get<string>("meta.directory_out") + conf.get<string>("meta.filename_out") + ".h5";
    cout << "Creating results file: " << output_file << endl;
    HDF hdf(output_file, HDF::DESTROY);

    // Write params to results file.
    hdf.writeXML("conf", conf);
    hdf.close();

    // Run solution procedure.
    if (conf.get<int>("debug.print") == 1) {
        cout << "Running solution procedure .." << endl;
    }
    StartSimulation<Vec2d>(conf, hdf);

    // End execution.
    t.addCheckPoint("end");
    prn(t.duration("start", "end"));
    cout << "Calculations saved to: " << output_file << "." << endl;

    return 0;
}
