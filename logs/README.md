# Log

## 02. 02. 2022

- Created project.
- Added files.

## 12. 02. 2022

- First draft.
- Added code.
- Added all results.

## 19. 02. 2022

- Added paper.
- Updated source code.
- Repository cleanup.