# A sharp-interface mesoscopic model for dendritic growth

[ICASP](https://icasp6.sciencesconf.org) conference paper where simulation of 2D dendrit growth using meshless methods is presented.

The project is closesly related to [this repository](https://gitlab.com/e62Lab/phase-change/-/tree/new).

## Abstract

The grain envelope model (GEM) describes the growth of envelopes of dendritic crystal grains during solidification. Numerically the growing envelopes are usually tracked using an interface capturing method employing a phase field equation on a fixed grid. Such an approach describes the envelope as a diffuse interface, which can lead to numerical artefacts that are possibly detrimental in describing highly nonlinear phenomena, such as the formation of new branches of the dendritic grains or initial stages of dendrite growth after nucleation. In this work, we present a sharp-interface formulation of the GEM that eliminates any such artefacts, and can thus track the envelope with high accuracy. The new formulation of the model uses an adaptive meshless discretization method to solve the diffusion in the liquid around the grains. We leverage the ability of the meshless method to operate on scattered nodes in order to accurately describe the interface, i.e., the grain envelope. The proposed algorithm combines parametric surface reconstruction, meshless discretization of parametric surfaces, and partial differential operator approximation using radial basis functions (RBF-FD). The approach is demonstrated on a two-dimensional h-adaptive solution of diffusive growth of dendrites and assessed by comparison to a conventional diffuse-interface fixed-grid GEM.

## Visuals

All visuals are in the `results/` directory. It might also be beneficial to check the `logs/`.

## Installation

Requirements:

- CMake
- C++ compiler
- Python 3.9 (or higher)
- Jupyter notebooks

## Usage

Create or go to `build/` directoy and build using

```bash
cmake .. && make -j 12
```

The executable will be created in `bin/` directory. The executable must be run with a parameter with all the settings, e.g.

```bash
./phase-change ../input/settings_proto.xml
```

## Support

Thanks to [E62 team](https://e6.ijs.si/parlab/) and **Miha Založnik** for support.

## Contributing

Entire [E62 team](https://e6.ijs.si/parlab/).

## Authors and contributors

- **Mitja Jančič** under supervision of
- **Miha Založnik** and
- **Gregor Kosec**

## Acknowledgments

The authors would like to acknowledge the financial support of the Slovenian Research Agency (ARRS) research core funding No. P2-0095 and the World Federation of Scientists.

# Reproducing paper results

To reproduce the paper results run

```bash
./phase-change ../input/solo.xml
```
> **Warning**: Simulation can take a while, approximately 35 hours.